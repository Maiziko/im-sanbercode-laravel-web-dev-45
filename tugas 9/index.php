
    <?php
    require('Animal.php');
    require('Frog.php');
    require('Ape.php');
        $sheep = new Animal("shaun");
        echo "Name : ".$sheep->get_name()."<br>";
        echo "legs : ".$sheep->get_legs()."<br>";
        echo "cold blooded : ".$sheep->get_cold_blooded()."<br>";
        echo "<br>";

		$Frog = new Frog("buduk");
        echo "Name : ".$Frog->get_name()."<br>";
        echo "legs : ".$Frog->get_legs()."<br>";
        echo "cold blooded : ".$Frog->get_cold_blooded()."<br>";
        echo "Jump : ";
        $Frog->jump();
        echo "<br>";

        $sungokong = new Ape("kera sakti");
        echo "Name : ".$sungokong->get_name()."<br>";
        echo "legs : ".$sungokong->get_legs()."<br>";
        echo "cold blooded : ".$sungokong->get_cold_blooded()."<br>";
        echo "Yell : ";
        $sungokong->yell();
    ?>